module NameHelpers

  def good_proj_names
    [ 'project_underscore', 'project-hyphens', 'project with spaces', 'project.with.periods', 'project1with2numbers' ]
  end

  def bad_proj_names
    [ 'projectname!', 'project@gitlab', 'project#1', 'project$100', 'project25%name', 'project^name', 'gitlab&project', 'project*name', 'project(name)', 'gitlab+project', 'project=project', ' projectname', '-projectname', 'project_name_over_255_characters_project_name_over_255_characters_project_name_over_255_characters_project_name_over_255_characters_project_name_over_255_characters_project_name_over_255_characters_project_name_over_255_characters_project_name_over_255_char' ]
  end
end

